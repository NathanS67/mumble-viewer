<?php
echo "<link rel=\"stylesheet\" href=\"mumviewer.css\">";

//Require Murmur-rest API running on port 8181 (https://github.com/alfg/murmur-rest)
$API = json_decode(file_get_contents("http://localhost:8181/servers/1/"), true);

echo "<div id=\"servername\">" . $API["name"] . "</div>";
foreach ($API["sub_channels"] as $channel) {
    echo "<div id=\"channel\">" . $channel["c"]["name"] . "</div>";
    foreach ($channel["users"] as $user) {
        if ($user["bytespersec"] > 0) {
            $userdetail = "<img id=\"speak\" src=\"./icons/talking_on.svg\" alt=\"speaking\">" . $user["name"];
        } else {
            $userdetail = "<img id=\"speak\" src=\"./icons/talking_off.svg\" alt=\"not speaking\">" . $user["name"];
        }

        if ($user["mute"]) {
            $userdetail .= "<img id=\"status\" src=\"./icons/muted_server.svg\" alt=\"server mute\">";
        }
        if ($user["selfMute"]) {
            $userdetail .= "<img id=\"status\" src=\"./icons/muted_self.svg\" alt=\"mute\">";
        }

        if ($user["deaf"]) {
            $userdetail .= "<img id=\"status\" src=\"./icons/deafened_server.svg\" alt=\"server deaf\">";
        }
        if ($user["selfDeaf"]) {
            $userdetail .= "<img id=\"status\" src=\"./icons/deafened_self.svg\" alt=\"deaf\">";
        }

        echo "<div id=\"user\">" . $userdetail . "</div>";
    }
}
